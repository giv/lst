<?php
require_once(__DIR__ . '/Vendor/php-standards/SplClassLoader.php');

$classLoader = new SplClassLoader('LightSoft', __DIR__ . '/Vendor');
$classLoader->register();

use LightSoft\ProblemFour\App;

$app = new App();
$app->run();