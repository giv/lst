<?php

namespace LightSoft\ProblemFour\Component;

use LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber;
use LightSoft\ProblemFour\Structure\ComplexNumber\Serializer;

class Storage {
    /**
    * Sets up instance properties, 
    * initiates the ComplexNumber serializer
    */
    public function __construct(\LightSoft\ProblemFour\PDO $db) {}

    /**
    * Loads raw data from the Database,
    * unserialize recieved data and creates new ComplexNumber instance
    * with unserialized Real and Imaginary parts.
    *
    * @return LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber
    */
    public function load() {}

    /**
    * Saves ComplexNumber instance in the Database,
    * at first we serialize ComplexNumber and then
    * save it to the Database.
    *
    * @param LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber 
    */
    public function save(ComplexNumber $number) {}
}