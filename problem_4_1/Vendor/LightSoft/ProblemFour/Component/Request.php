<?php

namespace LightSoft\ProblemFour\Component;

class Request {
    /**
    * Gets value from the input stream by key
    *
    * @param string $key The unique key of the value
    * @return mixed
    */
    public function get($key) {}
}