<?php

namespace LightSoft\ProblemFour\Structure\ComplexNumber;

class Serializer
{
    /**
    * Packs ComplexNumber to the string
    *
    * @param LightSoft\ProblemFour\Structure\ComplexNumber 
    */
    public function serialize(ComplexNumber $number){}

    /**
    * Unpacks ComplexNumber from the string
    *
    * @param string $str String representation of the ComplexNumber
    * @return LightSoft\ProblemFour\Structure\ComplexNumber
    */
    public function unserialize($str){}
}