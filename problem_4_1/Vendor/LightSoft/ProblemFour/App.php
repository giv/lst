<?php

namespace LightSoft\ProblemFour;

use LightSoft\ProblemFour\Component\Request;
use LightSoft\ProblemFour\Component\Response;
use LightSoft\ProblemFour\Component\Storage;
use LightSoft\ProblemFour\Component\Renderer;

use LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber;

class App
{
    /**
    *  Request instance 
    *
    * @var LightSoft\ProblemFour\Component\Request
    */
    private $rq;

    /**
    *  Storage instance 
    *
    * @var LightSoft\ProblemFour\Component\Storage
    */
    private $storage;

    /**
    *  Response instance 
    *
    * @var LightSoft\ProblemFour\Component\Response
    */
    private $response;

    /**
    *  Renderer instance 
    *
    * @var LightSoft\ProblemFour\Component\Renderer
    */
    private $renderer;

    /**
    * Initialize application props
    */
    public function __construct() {
        $this->rq = new Request();
        $this->storage = new Storage( new \PDO($dsn) );
        $this->response = new Response();
        $this->renderer = new Renderer();
    }

    /**
    * Executes the main application algorithm. 
    */
    public function run() {
        $A = new ComplexNumber($this->rq->get('x'), 
                               $this->rq->get('y'));
        $B = $this->storage->load();
        $C = $A->mul($B);

        $this->storage->save($C);
        $this->response->setContent($this->renderer->render($C))
                       ->send();
    }
}