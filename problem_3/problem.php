<?php

function isCorrect($expression) {
	if (empty($expression)) return true;
	if (strlen($expression) % 2 !== 0) return false;

	$openedBraces = new SplStack();

	for( $i=0; $i < strlen($expression); $i++ ) {
		$brace = $expression[$i];

		switch ($brace) {
			case '{':
			case '(':
				$openedBraces->push($brace);
				break;
			case '}':
			case ')':
				$prevBrace = $openedBraces->pop();
				if ($brace == '}' and $prevBrace != '{') return false;
				if ($brace == ')' and $prevBrace != '(') return false;
				break;
			default:
				return false;
		}
	}
	
	return true;
}

assert(isCorrect('') === true); 
assert(isCorrect('()}') === false);
assert(isCorrect('()') === true);
assert(isCorrect('{()}') === true);
assert(isCorrect('{()}{}') === true); 
assert(isCorrect('(())') === true); 
assert(isCorrect('{({({({()})})})}') === true); 
assert(isCorrect('{(})') === false);