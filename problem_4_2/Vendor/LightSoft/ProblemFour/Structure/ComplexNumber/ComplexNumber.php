<?php

namespace LightSoft\ProblemFour\Structure\ComplexNumber;

class ComplexNumber
{
    private $a;
    private $b;

    /**
    * Initialize real and imaginary parts
    */
    public function __construct($a, $b) {
        $this->a = $a;
        $this->b = $b;
    }

    /**
    * Multiply operation 
    * (a + bi)*(c + di) = (ac - bd) + (bc + ad)i
    *
    * @param ComplexNumber $z
    * @return ComplexNumber
    */
    public function mul(ComplexNumber $z) {
        $a = $this->a; $b = $this->b;
        $c = $z->getRe(); $d = $z->getIm();

        return new ComplexNumber(($a*$c - $b*$d), 
                                 ($b*$c + $a*$d));
    }

    /**
    * Gets the real part of the number
    *
    * @return int
    */
    public function getRe(){
        return $this->a;
    }

    /**
    * Gets the imaginary part of the number
    *
    * @return int
    */
    public function getIm(){
        return $this->b;
    }

    /**
    * Represents the instance as string 
    *
    * @return string
    */
    public function __toString() {
        return sprintf("%d + %di", $this->a, $this->b);
    }
}