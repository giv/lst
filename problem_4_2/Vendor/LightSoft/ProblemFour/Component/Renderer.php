<?php

namespace LightSoft\ProblemFour\Component;

class Renderer
{
    /**
    * Renders data onto the string representation
    * 
    * @param mixed $data Any data from the program
    * @return string
    */
    public function render($data) {}
}