<?php

namespace LightSoft\ProblemFour\Component\StorageAdapter;

class MySqlStorageAdapter implements StorageAdapterInterface 
{
    const LOCK_SH = 1; // read
    const LOCK_EX = 2; // write

    /**
    * A file resource
    */
    protected $fileName;

    /**
    * Constructor.
    *
    * Populates instance props.
    */
    public function __construct($fileName) {}

    /**
    * Reading raw data from the file.
    * It tries to acquire read lock before starting 
    * read from the file.
    *
    * @return mixed
    */
    public function load() {}
    
    /**
    * Saves item to the file and flushing it.
    * It tries to acquire write lock before starting 
    * write to the file.
    *
    * @param mixed 
    */
    public function save($item) {}

    /**
    * Acquires lock on the file
    * 
    * @param int $type Lock type
    */
    protected function acquireLock($type){}

    /**
    * Releases any type lock on the file
    */
    protected function releaseLock(){}
}