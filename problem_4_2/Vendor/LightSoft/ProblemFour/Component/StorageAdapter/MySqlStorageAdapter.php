<?php

namespace LightSoft\ProblemFour\Component\StorageAdapter;

class MySqlStorageAdapter implements StorageAdapterInterface 
{
    /**
    * Database
    *
    * @var /PDO
    */
    protected $db;

    /**
    * Constructor.
    *
    * Populates instance props.
    */
    public function __construct(PDO $db) {}

    /**
    * Loads raw data from storage.
    *
    * @return mixed
    */
    public function load() {}
    
    /**
    * Saves item to the storage.
    *
    * @param mixed 
    */
    public function save($item) {}
}