<?php

namespace LightSoft\ProblemFour\Component\StorageAdapter;

interface StorageAdapterInterface
{
    /**
    * Loads raw data from storage.
    *
    * @return mixed
    */
    public function load() {}

    /**
    * Saves item to the storage.
    *
    * @param mixed 
    */
    public function save($item) {}
}