<?php

namespace LightSoft\ProblemFour\Component\StorageAdapter;

use LightSoft\ProblemFour\Component\Request\RequestInterface;
use LightSoft\ProblemFour\Structure\ComplexNumber\Serializer;

class InputStreamAdapter implements StorageAdapterInterface 
{
    protected $rq;

    /**
    * ComplexNumber Serializer instance
    *
    * @var LightSoft\ProblemFour\Structure\ComplexNumber\Serializer
    */
    protected $marshal;

    /**
    * Constructor.
    */
    public function __construct(RequestInterface $rq, 
                                Serializer $serilizer) 

    /**
    * Gets params from the Request and serialize 
    * it before return.
    *
    * @return string Serialized ComplexNumber
    */
    public function load(){}

    /**
    * Nothing to do
    */
    public function save($item) { return; }
}