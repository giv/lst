<?php

namespace LightSoft\ProblemFour\Component\Response;

interface ResponseInterface {
    protected $content;

    /**
    * Sets the response body
    *
    * @param string $content Body for the response
    */
    public function setContent($content) {}

    /**
    * Sends the body to the output
    */
    public function send() {}
}