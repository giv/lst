<?php

namespace LightSoft\ProblemFour\Component\Response;

class CliResponse implements Response {
    protected $stream;
    protected $content;

    /**
    * Constructor.
    *
    * @param mixed $stream A stream resource
    */
    public function __construct($stream)
    
    /**
    * Sets the response body
    *
    * @param string $content Body for the response
    */
    public function setContent($content) {}

    /**
    * Writes body to the stream
    */
    public function send() {}
}