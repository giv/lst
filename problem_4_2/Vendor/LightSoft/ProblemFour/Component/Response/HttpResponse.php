<?php

namespace LightSoft\ProblemFour\Component\Response;

class Response {
    protected $headers = array();
    protected $content;

    /**
    * Sets the response body
    *
    * @param string $content Body for the response
    */
    public function setContent($content) {}

    /**
    * Adds header to the response
    *
    * @param string $name Name of the header
    * @param string $value Value of the header
    */
    public function addHeader($name, $value) {}

    /**
    * Sends the headers and body to the browser
    */
    public function send() {}
}