<?php

namespace LightSoft\ProblemFour\Component;

use LightSoft\ProblemFour\Component\StorageAdapter\StorageAdapterInterface;
use LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber;
use LightSoft\ProblemFour\Structure\ComplexNumber\Serializer;

class Storage {
    /**
    * Sets up instance properties, 
    * initiates the ComplexNumber serializer
    */
    public function __construct(StorageAdapterInterface $adapter, Serializer $serializer) {}

    /**
    * Loads raw data via adapter,
    * unserialize recieved data and creates new ComplexNumber instance
    * with unserialized Real and Imaginary parts.
    *
    * @return LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber
    */
    public function load() {}

    /**
    * Saves ComplexNumber instance to the storage via adapter,
    * at first we serialize ComplexNumber and then
    * save it to the Database.
    *
    * @param LightSoft\ProblemFour\Structure\ComplexNumber\ComplexNumber 
    */
    public function save(ComplexNumber $number) {}
}