<?php

namespace LightSoft\ProblemFour\Component\Request;

class CliRequest implements RequestInterface {
    /**
    * Gets value from the $_SERVER['argv'] by key
    *
    * @param string $key The unique key of the value
    * @return mixed
    */
    public function get($key) {}
}