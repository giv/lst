<?php

namespace LightSoft\ProblemFour\Component\Request;

class HttpRequest implements RequestInterface {
    /**
    * Gets value from the $_REQUEST by key
    *
    * @param string $key The unique key of the value
    * @return mixed
    */
    public function get($key) {}
}