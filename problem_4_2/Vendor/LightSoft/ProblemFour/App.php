<?php

namespace LightSoft\ProblemFour;

use LightSoft\ProblemFour\Component\Response\ResponseInterface;
use LightSoft\ProblemFour\Component\Storage;
use LightSoft\ProblemFour\Component\Renderer;

class App
{
    /**
    *  Storage instance with inputstream adapter 
    *
    * @var LightSoft\ProblemFour\Component\Storage
    */
    private $rq;

    /**
    *  Storage instance with file adapter
    *
    * @var LightSoft\ProblemFour\Component\Storage
    */
    private $storage;

    /**
    *  Response instance 
    *
    * @var LightSoft\ProblemFour\Component\ResponseInterface
    */
    private $response;

    /**
    *  Renderer instance 
    *
    * @var LightSoft\ProblemFour\Component\Renderer
    */
    private $renderer;

    /**
    * Initialize application props
    */
    public function __construct(Storage $rq, Storage $storage, 
                                ResponseInterface $response, 
                                Renderer $renderer) {
        $this->rq = $rq;
        $this->storage = $storage;
        $this->response = $response;
        $this->renderer = $renderer;
    }

    /**
    * Executes the main application algorithm. 
    */
    public function run() {
        $A = $this->rq->load();
        $B = $this->storage->load();
        $C = $A->mul($B);

        $this->storage->save($C);
        $this->response->setContent($this->renderer->render($C))
                       ->send();
    }
}