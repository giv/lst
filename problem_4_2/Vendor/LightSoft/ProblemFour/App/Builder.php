<?php

namespace LightSoft\ProblemFour\App;

use LightSoft\ProblemFour\App;
use LightSoft\ProblemFour\Component\Response\ResponseInterface;
use LightSoft\ProblemFour\Component\Storage;
use LightSoft\ProblemFour\Component\Renderer;

class AppBuilder
{
    protected $rq;
    protected $store;
    protected $resp;
    protected $renderer;

    /**
    * Sets the Storage instance for the App 
    *
    * @param LightSoft\ProblemFour\Component\Storage
    * @return  LightSoft\ProblemFour\App\AppBuilder
    */
    public function request(Storage $rq)

    /**
    * Sets the Storage instance for the App 
    *
    * @param LightSoft\ProblemFour\Component\Storage
    * @return  LightSoft\ProblemFour\App\AppBuilder
    */
    public function storage(Storage $store)

    /**
    * Sets the Storage instance for the App 
    *
    * @param LightSoft\ProblemFour\Component\Responce\ResponseInterface
    * @return  LightSoft\ProblemFour\App\AppBuilder
    */
    public function response(ResponseInterface $resp)

    /**
    * Sets the Storage instance for the App 
    *
    * @param LightSoft\ProblemFour\Component\Renderer
    * @return  LightSoft\ProblemFour\App\AppBuilder
    */
    public function renderer(Renderer $renderer)

    /**
    * Bulds App instance using defined components
    *
    * @return LightSoft\ProblemFour\App
    */
    public function build()
}