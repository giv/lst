<?php

namespace LightSoft\ProblemFour\App;

use LightSoft\ProblemFour\App\Builder;
use LightSoft\ProblemFour\App\Env\HttpEnvFactory;
use LightSoft\ProblemFour\App\Env\CliEnvFactory;

class Runner
{
    /**
    * Creates App instance using Builder 
    * and Environment and runing it.
    */
    public static function run() {
        $builder = new Builder;

        $env = self::getEnvFactory()
        $app = $builder->request($env->getRequest())
                       ->storage($env->getStorage())
                       ->response($env->getResponse())
                       ->renderer($env->getRenderer())
                       ->build();

        $app->run();
    }

    /**
    * Gets EnvironmentFactory for 
    * the current PHP runing mode
    *
    * @return LightSoft\ProblemFour\App\Env\EnvFactory
    */
    public static function getEnvFactory() {
        if(php_sapi_name() == "cli") {
            return new CliEnvFactory()
        } else {
            return new HttpEnvFactory()
        }
    }
}