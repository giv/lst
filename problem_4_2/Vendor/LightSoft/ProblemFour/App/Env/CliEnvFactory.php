<?php

namespace LightSoft\ProblemFour\App\Env;

use LightSoft\ProblemFour\Component\Storage;
use LightSoft\ProblemFour\Component\Renderer;
use LightSoft\ProblemFour\Component\Request\CliRequest;
use LightSoft\ProblemFour\Component\Response\CliResponse;
use LightSoft\ProblemFour\Component\StorageAdapter\InputStreamAdapter;
use LightSoft\ProblemFour\Component\StorageAdapter\FileStorageAdapter;
use LightSoft\ProblemFour\Structure\ComplexNumber\Serializer;

class CliEnvFactory extends EnvFactory
{
    /**
    * @return LightSoft\ProblemFour\Component\Storage
    */
    public function getRequest() {
        return new Storage(
                    new InputStreamAdapter(new CliRequest, new Serializer),
                    new Serializer
               );
    }

    /**
    * @return LightSoft\ProblemFour\Component\Storage
    */
    public function getStorage() {
        return new Storage(
                    new FileStorageAdapter(''),
                    new Serializer
               );
    }
    
    /**
    * @return LightSoft\ProblemFour\Component\Response\CliResponse
    */
    public function getResponse() {
        return new CliResponse(fopen('php://stderr', 'w'));
    }
    
    /**
    * @return LightSoft\ProblemFour\Component\Renderer
    */
    public function getRenderer() {
        return new Renderer;
    }
}