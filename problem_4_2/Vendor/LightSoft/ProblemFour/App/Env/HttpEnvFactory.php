<?php

namespace LightSoft\ProblemFour\App\Env;

use LightSoft\ProblemFour\Component\Storage;
use LightSoft\ProblemFour\Component\Renderer;
use LightSoft\ProblemFour\Component\Request\HttpRequest;
use LightSoft\ProblemFour\Component\Response\HttpResponse;
use LightSoft\ProblemFour\Component\StorageAdapter\InputStreamAdapter;
use LightSoft\ProblemFour\Component\StorageAdapter\MySqlStorageAdapter;
use LightSoft\ProblemFour\Structure\ComplexNumber\Serializer;

class HttpEnvFactory extends EnvFactory
{
    /**
    * @return LightSoft\ProblemFour\Component\Storage
    */
    public function getRequest() {
        return new Storage(
                    new InputStreamAdapter(new HttpRequest, new Serializer),
                    new Serializer
               );
    }

    /**
    * @return LightSoft\ProblemFour\Component\Storage
    */
    public function getStorage() {
        return new Storage(
                    new MySqlStorageAdapter( new PDO ),
                    new Serializer
               );
    }
    
    /**
    * @return LightSoft\ProblemFour\Component\Response\HttpResponse
    */
    public function getResponse() {
        return new HttpResponse;
    }
    
    /**
    * @return LightSoft\ProblemFour\Component\Renderer
    */
    public function getRenderer() {
        return new Renderer;
    }
}