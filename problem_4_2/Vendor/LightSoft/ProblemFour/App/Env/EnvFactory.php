<?php

namespace LightSoft\ProblemFour\App\Env;

abstract class EnvFactory
{
    public abstract function getRequest();
    public abstract function getStorage();
    public abstract function getResponse();
    public abstract function getRenderer();
}